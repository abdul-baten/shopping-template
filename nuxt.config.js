const pkg = require('./package')
const webpack = require('webpack');

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fe6500' },

  /*
  ** Global CSS
  */
    css: [
        { src: '~/assets/css/style.css', lang: 'css' },
        { src: '~/assets/css/font-icons.css', lang: 'css' },
        { src: '~/assets/css/responsive.css', lang: 'css' }
    ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      '~/plugins/firebase/init.js',
      '~/plugins/mixin.js',
      '~/plugins/element-ui.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [,
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ],

  /*
  ** Build configuration
  */
  build: {
      extractCSS: true,
    /*
    ** You can extend webpack config here
    */
      vendor: ['jquery'],
      plugins: [
          new webpack.ProvidePlugin({
              '$': 'jquery',
              'jQuery': 'jquery',
              'window.jQuery': 'jquery',
              'window.$': 'jquery',
              'moment': 'moment'
          })
      ],
      extend: function (config, {isDev, isClient}) {

          config.node = {

              fs: "empty"
          };
      }
  }
}
