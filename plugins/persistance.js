const store = require('store');
const Cryptr = require('cryptr');

export default {
    cyclicsCleaner: function () {
        var seenObjects = [];
        return function cleanCyclics(key, obj) {
            if (obj && typeof obj === 'object') {
                if (seenObjects.indexOf(obj) !== -1) {
                    return null;
                }
                seenObjects.push(obj);
            }
            return obj;
        }
    },

    checkExistingUser: () => {
        let aladin = store.get('d_aladin');
        //console.log('current aladin1 : ' , store.get());
        if (aladin !== undefined && aladin.length > 0) {
            var cryptr = new Cryptr(aladin);


            try {

                const decryptedUser = cryptr.decrypt(store.get('d_aladins_info'));
                const decryptedTime = cryptr.decrypt(store.get('d_aladins_valid'));

                return {
                    userInfo: JSON.parse(decryptedUser),
                    expire: JSON.parse(decryptedTime)
                }


            } catch (e) {
                console.log('Not valid here : ' , e)
                return {
                    userInfo: {},
                    expire: 0
                }

            }
        }



        return {
            userInfo: {},
            expire: 0
        }
    },

    validateOldUser : (instance , info) => {
        if((new Date()).getTime() > info.expire){
            instance.$store.dispatch('auth/logout')
        } else {
            instance.$store.dispatch('auth/setUser' , info.userInfo);
            instance.$store.dispatch('auth/setUserExpire' , info.expire);
        }
    },

    checkExistingCountry : () => {
        let aladin = store.get('rand');
        if (aladin !== undefined && aladin.length > 0) {
            var cryptr = new Cryptr(aladin);

            const decryptedCountry = cryptr.decrypt(store.get('country_info'));
            try {
                return {
                    countryInfo: JSON.parse(decryptedCountry),
                }


            } catch (e) {
                //console.log('Not valid : ' , e)
                return {
                    countryInfo: {}
                }

            }
        }



        return {
            countryInfo: {}
        }
    },

    validateOldCountry : (instance , info) => {
        if(info.countryInfo.hasOwnProperty('postcode')){
            instance.$store.dispatch('su/setCountry' , info.countryInfo);
            instance.$store.dispatch('su/setPostcodeToRedirect' , false);
        }else{
            instance.$store.dispatch('su/setPostcodeToRedirect' , true);
        }
    },

    suffleArrayObject: (arrObject , oldIndex , newIndex) => {

        /*
         arrObject format should be this
            [
                { id : 'sdfgsd' , order : 45 },
                { id : 'sdfdfggsd' , order : 50 },
            ]
         */


        const cloned = JSON.parse(JSON.stringify(arrObject));


        var primaryArray = arrObject.map((item) => item.id);

        var resultArray = [];

        for(var i = 0 ;i < primaryArray.length ; i++){
            resultArray[i] = primaryArray[i];
        }




        if(newIndex < oldIndex){
            //console.log('go up');
            for(var i = newIndex ; i < primaryArray.length-1 ; i++){

                if(i < oldIndex) {
                    resultArray[i + 1] = cloned[i].id;
                }
            }
            resultArray[newIndex] = cloned[oldIndex].id;
        } else {
            //console.log('go down');
            for(var i = oldIndex ; i < primaryArray.length-1 ; i++){
                if(i < newIndex)
                    resultArray[i] = cloned[i+1].id;
            }
            resultArray[newIndex] = cloned[oldIndex].id;
        }

        let finalSorted = [];

        resultArray.forEach((item , index) => {
            finalSorted.push({
                id : item,
                order : cloned[index].order
            })
        })

        return finalSorted;
    },

    generateRandomChar: (size) => {
        var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //from where to create
        var i=0, ret='';
        while(i++<size){
            ret += charset.charAt(Math.random() * charset.length)
        }
        return ret;
    }

}