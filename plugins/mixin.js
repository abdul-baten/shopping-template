import Vue from 'vue'
import persistance from '~/plugins/persistance';

import DB from '~/plugins/firebase/init'
import axios from 'axios'

Vue.mixin({
    methods : {
        signout(){
            let self = this;
            this.$store.dispatch('auth/logout').then(({success})=> {
                if(success === true){
                    self.$router.push('/');
                }
            });
        },
        validateOldUser(){
            var info = persistance.checkExistingUser();
            if((new Date()).getTime() > info.expire){
                console.log('validation failed. logging user out')
                this.$store.dispatch('auth/logout').then(data => {
                    if(data.success === true){
                        this.$router.push({path :  `/`})
                    }
                })
            } else {
                this.$store.dispatch('auth/setUser' , info.userInfo);
                this.$store.dispatch('auth/setUserExpire' , info.expire);
            }

            if(!this.user.hasOwnProperty('email')){
                this.signout();
            }
        },
        validateForPassword(){
            var info = persistance.checkExistingUser();
            console.log(info);
            if(0 !== info.expire){
                console.log('here')
                this.validateOldUser()

            } else {
                console.log('there')

            }
        },
        removeReferences(object){
            return JSON.parse(JSON.stringify(object))
        },
        getConceirgeFee(){
            return 5;
        },

        deliveryFee(){
            return 5;
        },

        freeDeliveryFor(){
            return 80;
        },

        roundNum2 (value){
            return Math.round(value * 100) / 100;
        },

        getMiliseconds(day){
            return 1000 * 60 * 60 * 24 * day;
        },

        sendEmail(data){
            let user = {};
            DB.collection("users").where('uid' , '==' , data.uid).get().then(function (querySnapshot) {

                if (!querySnapshot.empty) {
                    querySnapshot.forEach(doc => {
                        user = doc.data();
                    });

                    if(user.email !== undefined && user.email !== ''){
                        axios({
                            method:'post',
                            url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendEmail',
                            data: {
                                MailTo: user.email,
                                name : user.name,
                                orderID : data.orderID,
                                type : data.type
                            }
                        })
                            .then(function(response) {
                               // console.log(response.data);
                            });
                    }

                }

            }).catch(function (error) {
                console.log("Error getting document:", error);
            });
        },

        sendCancelEmail(order){
            let self= this;
            let storeIDS = Object.keys(order.orderStatus) , users = [] , customer = {};


            DB.collection("users").get().then(querySnapshot => {
                if (!querySnapshot.empty) {
                    querySnapshot.forEach(doc => {
                        if(storeIDS.includes(doc.data().shopID))
                            users.push(doc.data());

                        if(doc.data().uid == order.customer){
                            customer = doc.data();
                        }
                    });

                    users.forEach(item => {
                        axios({
                            method: 'post',
                            url: 'https://us-central1-aladins-chirag.cloudfunctions.net/orderCancelEmailStore',
                            data: {
                                name : item.name,
                                orderID : order.id,
                                mailTo : item.email
                            }
                        })
                            .then(function (response) {

                            }).catch(err => {
                            console.log(err);
                        });
                    })


                    axios({
                        method:'post',
                        url:'https://us-central1-aladins-chirag.cloudfunctions.net/sendEmail',
                        data: {
                            MailTo: customer.email,
                            name : customer.name,
                            orderID : order.id,
                            type : 'cancel'
                        }
                    })
                        .then(function(response) {
                            // console.log(response.data);
                        });



                }
            })
        },
        referralImplement(order){
            axios({
                method:'post',
                url:'https://us-central1-aladins-chirag.cloudfunctions.net/referralCalculation',
                data: {
                    order: order
                }
            })
                .then(function(response) {
                    // console.log(response.data);
                });
        },
        storeBanEmail(data){
            axios({
                method:'post',
                url:'https://us-central1-aladins-chirag.cloudfunctions.net/storeAcceptance',
                data: {
                    name: data.name,
                    MailTo : data.MailTo,
                    mailText : data.mailText,
                    type : data.type
                }
            })
                .then(function(response) {
                    // console.log(response.data);
                });
        }
    },
    computed : {
        user(){
            return this.$store.getters['auth/User'];
        },
    }
})