import Vue from 'vue'

Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };
if(!Date.now) Date.now = function() { return new Date(); }
Date.time = function() { return Date.now().getUnixTime(); }

// const ElementUI = require('element-ui')
// import "element-ui/lib/theme-chalk/index.css";
// import "assets/css/custom.css";
// const locale = require('element-ui/lib/locale/lang/en')
// Vue.use(ElementUI, { locale })

const ElementUI = require('element-ui')
import "element-ui/lib/theme-chalk/index.css";
import "assets/css/custom.css";
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';

// configure language
locale.use(lang);

// Init ElementUI components
Vue.use(ElementUI);