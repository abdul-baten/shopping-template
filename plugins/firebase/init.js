import firebase from 'firebase';
import firebaseConfig  from'./config';

let apiKey = ''
if (process.browser) {
    window.onNuxtReady((context) => {
        //console.log(context)
        //console.log(context.$store.getters.api.firebase)
        apiKey = context.$store.getters.api.firebase
        //store1 = $store
    })
}


// export default ({ app }, inject) => {
//     //console.log(app.store.getters);
//     // Set the function directly on the context.app object
//
//     firebaseConfig.apiKey = app.store.getters.api.firebase;
//
//
// }

// export default  firebaseApp.firestore();

// if(apiKey !== ''){
//     firebaseConfig.apiKey = apiKey
// }
// console.log("api : " ,apiKey)

if (!firebase.apps.length) {
    var firebaseApp = firebase.initializeApp(firebaseConfig);
}
else{
    var firebaseApp = firebase.app()
}


/* THIS PART IS FOR FUTURE FEATURE RECOMMENDED BY FIRESTORE */
const firestore = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);


/* ----- */


export default firebaseApp.firestore();