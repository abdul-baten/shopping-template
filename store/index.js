import axios from 'axios'
export const state = () => ({
    flash : { message : '' , type: '' },
    api : {}
})
export const getters = {
    flash(state){
        return state.flash;
    },
    api(state){
        return state.api
    }
}

export const actions = {
    nuxtServerInit(vuexContext , nuxtContext){

        // return new Promise((resolve, reject) => {
        //                 //vuexContext.commit("setOk", {me : true});
        //                 resolve();
        //
        //         });

        //  return axios({
        //     method:'post',
        //     url:'https://us-central1-aladins-chirag.cloudfunctions.net/getAPIs',
        //     data: {
        //         storeID : ''
        //     }
        // })
        //     .then(function(response) {
        //
        //          vuexContext.commit('setApi' , response.data)
        //
        //     }).catch(err => {
        //     console.log(err);
        // });
    },
    flash(context , payload){
        context.commit('flash' , payload)
    },
}

export const mutations = {
    flash(state , payload){
        state.flash = payload;
    },
    setApi(state , payload){
        state.api = payload
    }

}

