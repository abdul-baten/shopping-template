const store = require('store');
const Cryptr = require('cryptr');
import firebase from 'firebase'

export const state = () => ({
    user : {},
    expire : 0
})
export const getters = {
    User(state){
        return state.user;
    }

}

export const actions = {
    switchRole(context , payload){

        return new Promise((resolve , reject) => {
            let info = {...context.state.user , type : payload.role , shopID : payload.shopID , name : payload.shopName};
            let cryptr = new Cryptr(info.uid);
            const encryptedString = cryptr.encrypt(JSON.stringify(info));

            store.set('d_aladins_info', encryptedString);
            store.set('d_aladin', info.uid);

            context.commit('setUser' , info);

            resolve({shopID : payload.shopID});
        })


    },
    setUser(context , payload){
        context.commit('setUser' , payload)
    },
    setUserExpire(context , payload){
        context.commit('setUserExpire' , payload)
    },
    logout(context){

        return firebase.auth().signOut().then(() => {
            context.commit('logout');

            return { success : true }
        });
        // return new Promise((resolve , reject) => {
        //     context.commit('logout');
        //     context.dispatch('anonymous');
        //     resolve({ success : true })
        // })
    },
    anonymous(context){
    }

}

export const mutations = {
    setUser(state , payload){
        state.user = {...payload};
    },
    setUserExpire(state , payload){
        state.expire = payload;
    },
    logout(state , payload){
        state.user = {};
        state.expire = 0;

        store.set('aladins_info', '');
        store.set('d_aladins_info', '');
        store.set('aladin', '');
        store.set('d_aladin', '');
        store.set('aladins_valid' , '');
        store.set('d_aladins_valid' , '');


    }

}

